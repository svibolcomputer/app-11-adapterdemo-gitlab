package com.example.app_11_adapterdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button mBtnDefaultAdapter = findViewById(R.id.btn_default_adapter);
        mBtnDefaultAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DefaultAdapterWithLVActivity.class);
                intent.putExtra("kDisplay", "Default adapter with list view");
                startActivity(intent);
            }
        });

        Button mBtnCustomAdapter = findViewById(R.id.btn_custom_adapter);
        mBtnCustomAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CustomAdapterActivity.class);
                intent.putExtra("kDisplay", "Custom adapter with list view");
                startActivity(intent);
            }
        });

        Button mBtnCustomAdapterModel = findViewById(R.id.btn_custom_adapter_with_model);
        mBtnCustomAdapterModel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CustomAdapterWithModelActivity.class);
                intent.putExtra("kDisplay", "Custom Adapter with Model");
                startActivity(intent);
            }
        });



    }
}
