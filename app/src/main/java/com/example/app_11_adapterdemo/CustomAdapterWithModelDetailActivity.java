package com.example.app_11_adapterdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomAdapterWithModelDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_adapter_with_model_detail);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            ImageView mImvProfilePic    = findViewById(R.id.img_profile);
            TextView mTvBrandName       = findViewById(R.id.tv_brand_name);
            TextView mTvOrigin          = findViewById(R.id.tv_origin);
            TextView mTvDescription     = findViewById(R.id.tv_description);

            mImvProfilePic.setImageResource(bundle.getInt("kProfilePic"));
            mTvBrandName.setText(bundle.getString("kBrandName"));
            mTvOrigin.setText(bundle.getString("kOrigin"));
            mTvDescription.setText(bundle.getString("kDescription"));
        }
    }
}
