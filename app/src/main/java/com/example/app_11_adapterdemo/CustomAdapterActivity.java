package com.example.app_11_adapterdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.app_11_adapterdemo.custom.CustomAdapter;

public class CustomAdapterActivity extends AppCompatActivity {

    private int[] mProfilePic = {   R.drawable.c_hiace,
                                    R.drawable.c_lexus450h,
                                    R.drawable.c_lexus570,
                                    R.drawable.c_prius,
                                    R.drawable.c_revo};

    private String[] mName = {      "Toyota Hiace",
                                    "Lexus RX 450h",
                                    "Lexus LX 570",
                                    "Toyota Prius",
                                    "Toyota Revo"};

    private String[] mPrice = {     "$ 100.00",
                                    "$ 250.00",
                                    "$ 350.50",
                                    "$ 150.90",
                                    "$ 230.10"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_adapter);

        Bundle extra = getIntent().getExtras();
        if (extra != null){
            TextView mTvDisplay = findViewById(R.id.tv_display);
            mTvDisplay.setText(extra.getString("kDisplay"));
        }

        ListView mLvItems = findViewById(R.id.lv_items);

        CustomAdapter customAdapter1 = new CustomAdapter(CustomAdapterActivity.this, mProfilePic, mName, mPrice);

        mLvItems.setAdapter(customAdapter1);

        mLvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(CustomAdapterActivity.this, CustomAdapterDetailActivity.class);
                intent.putExtra("kPosition",position);
                startActivity(intent);
            }
        });
    }
}
