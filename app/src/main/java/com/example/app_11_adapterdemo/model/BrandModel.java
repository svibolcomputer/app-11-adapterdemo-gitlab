package com.example.app_11_adapterdemo.model;

public class BrandModel {
    private int imgProfile;
    private String strBrandName;
    private String strOrigin;
    private String strDescription;

    public BrandModel(int imgProfile, String strBrandName, String strOrigin, String strDescription) {
        this.imgProfile     = imgProfile;
        this.strBrandName   = strBrandName;
        this.strOrigin      = strOrigin;
        this.strDescription = strDescription;
    }

    public int getImgProfile() {
        return imgProfile;
    }

    public void setImgProfile(int imgProfile) {
        this.imgProfile = imgProfile;
    }

    public String getStrBrandName() {
        return strBrandName;
    }

    public void setStrBrandName(String strBrandName) {
        this.strBrandName = strBrandName;
    }

    public String getStrOrigin() {
        return strOrigin;
    }

    public void setStrOrigin(String strOrigin) {
        this.strOrigin = strOrigin;
    }

    public String getStrDescription() {
        return strDescription;
    }

    public void setStrDescription(String strDescription) {
        this.strDescription = strDescription;
    }
}
