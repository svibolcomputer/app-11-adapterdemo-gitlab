package com.example.app_11_adapterdemo.custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app_11_adapterdemo.R;

public class CustomAdapter extends BaseAdapter {

    private Context context;
    private int[] arrProfile;
    private String[] arrName;
    private String[] arrPrice;

    public CustomAdapter(Context context, int[] arrProfile, String[] arrName, String[] arrPrice) {
        this.context    = context;
        this.arrProfile = arrProfile;
        this.arrName    = arrName;
        this.arrPrice   = arrPrice;
    }

    @Override
    public int getCount() {
        return arrName.length;
    }

    @Override
    public Object getItem(int position) {
        return arrName[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(context).inflate(R.layout.custom_contact_layout,null);

        ImageView mImgProfile   = convertView.findViewById(R.id.imv_profile);
        TextView mTvName        = convertView.findViewById(R.id.tv_header);
        TextView mTvPhone       = convertView.findViewById(R.id.tv_header_sub);

        mImgProfile.setImageResource(arrProfile[position]);
        mTvName.setText(arrName[position]);
        mTvPhone.setText(arrPrice[position]);

        return convertView;
    }
}
