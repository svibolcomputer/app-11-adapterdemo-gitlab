package com.example.app_11_adapterdemo.custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.app_11_adapterdemo.R;
import com.example.app_11_adapterdemo.model.BrandModel;

import java.util.ArrayList;
import java.util.List;

public class CustomAdapterWithModel extends BaseAdapter {

    private Context context;
    List<BrandModel> objBandModel = new ArrayList<>();

    public CustomAdapterWithModel(Context context, List<BrandModel> objBandModel) {
        this.context = context;
        this.objBandModel = objBandModel;
    }

    @Override
    public int getCount() {
        return objBandModel.size();
    }

    @Override
    public Object getItem(int position) {
        return objBandModel.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.custom_contact_layout, null);

        ImageView mImvBrandPic  = convertView.findViewById(R.id.imv_profile);
        TextView mTvBrandName   = convertView.findViewById(R.id.tv_header);
        TextView mTvOrigin      = convertView.findViewById(R.id.tv_header_sub);

        mImvBrandPic.setImageResource(objBandModel.get(position).getImgProfile());
        mTvBrandName.setText(objBandModel.get(position).getStrBrandName());
        mTvOrigin.setText(objBandModel.get(position).getStrOrigin());

        return convertView;
    }
}
