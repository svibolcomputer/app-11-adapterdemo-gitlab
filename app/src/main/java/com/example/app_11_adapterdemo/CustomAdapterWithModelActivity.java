package com.example.app_11_adapterdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.app_11_adapterdemo.custom.CustomAdapterWithModel;
import com.example.app_11_adapterdemo.model.BrandModel;

import java.util.ArrayList;
import java.util.List;

public class CustomAdapterWithModelActivity extends AppCompatActivity {

    private int[] mProfilePic = {
            R.drawable.b_ford,
            R.drawable.b_honda,
            R.drawable.b_lexus,
            R.drawable.b_mazda,
            R.drawable.b_mitrubishi,
            R.drawable.b_nissan,
            R.drawable.b_toyota};

    private String[] mName = {
            "Ford",
            "Honda",
            "Lexus",
            "Mazda",
            "Mitsubishi",
            "Nissan",
            "Totota"};

    private String[] mOrigin = {
            "USA",
            "JAPAN",
            "ARAB",
            "USA",
            "JAPAN",
            "JAPAN",
            "JAPAN"};

    private BrandModel brandModel;
    List<BrandModel> brandModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_adapter_with_model);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null){
            TextView mTvDisplay = findViewById(R.id.tv_display);
            mTvDisplay.setText(bundle.getString("kDisplay"));
        }

        ListView mLvBrand = findViewById(R.id.lv_brand_items);

        brandModelList = new ArrayList<>();

        for (int i = 0; i < 7; i++ ){
            brandModel = new BrandModel(mProfilePic[i], mName[i], mOrigin[i],"Description "+ i);
            brandModelList.add(brandModel);
        }

        CustomAdapterWithModel customAdapterWithModel = new CustomAdapterWithModel(CustomAdapterWithModelActivity.this, brandModelList);

        mLvBrand.setAdapter(customAdapterWithModel);

        mLvBrand.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                brandModel = (BrandModel)parent.getItemAtPosition(position);

                int intPic = brandModel.getImgProfile();
                String strBrandname = brandModel.getStrBrandName();
                String strOrigin = brandModel.getStrOrigin();
                String strDescription = brandModel.getStrDescription();

//                Toast.makeText(CustomAdapterWithModelActivity.this, strBrandname +":"+ strOrigin, Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(CustomAdapterWithModelActivity.this, CustomAdapterWithModelDetailActivity.class);
                intent.putExtra("kProfilePic",intPic);
                intent.putExtra("kBrandName",strBrandname);
                intent.putExtra("kOrigin",strOrigin);
                intent.putExtra("kDescription",strDescription);
                startActivity(intent);
            }
        });

    }
}
