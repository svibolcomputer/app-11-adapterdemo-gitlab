package com.example.app_11_adapterdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DefaultAdapterWithLVActivity extends AppCompatActivity {
    TextView mTvItem;
    ListView mLvItems;
    String[] arrItems = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default_adapter_with_lv);

        Bundle extra = getIntent().getExtras();
        if (extra != null){
            TextView mTvDisplay = findViewById(R.id.tv_display);
            mTvDisplay.setText(extra.getString("kDisplay"));
        }

        mLvItems    = findViewById(R.id.lv_items);
        mTvItem     = findViewById(R.id.tv_item);
        ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<>(DefaultAdapterWithLVActivity.this, R.layout.tv_layout, R.id.tv_item, arrItems);

        mLvItems.setAdapter(arrayAdapter1);

        mLvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(DefaultAdapterWithLVActivity.this, position + " : " + parent.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
