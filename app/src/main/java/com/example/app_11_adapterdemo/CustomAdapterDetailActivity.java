package com.example.app_11_adapterdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomAdapterDetailActivity extends AppCompatActivity {

    private int[] mProfilePic = {
            R.drawable.c_hiace,
            R.drawable.c_lexus450h,
            R.drawable.c_lexus570,
            R.drawable.c_prius,
            R.drawable.c_revo};

    private String[] mName = {
            "Toyota Hiace",
            "Lexus RX 450h",
            "Lexus LX 570",
            "Toyota Prius",
            "Toyota Revo"};

    private String[] mPrice = {
            "$ 100.00",
            "$ 250.00",
            "$ 350.50",
            "$ 150.90",
            "$ 230.10"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_adapter_detail);

        Bundle extra = getIntent().getExtras();
        int position = -1;
        if(extra != null){
            position = extra.getInt("kPosition");
        }

        ImageView mImvProfile   = findViewById(R.id.img_profile);
        TextView mTvName        = findViewById(R.id.tv_product_name);
        TextView mTvPrice       = findViewById(R.id.tv_price);
        TextView mTvDescription = findViewById(R.id.tv_description);

        mImvProfile.setImageResource(mProfilePic[position]);
        mTvName.setText(mName[position]);
        mTvPrice.setText(mPrice[position]);
        mTvDescription.setText(""+ position);





    }
}
